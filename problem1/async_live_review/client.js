const fetch = require("node-fetch");

async function getUsers() {
  const response = await fetch("http://localhost:3000/users");
  const usersData = await response.json();
  return usersData;
}

async function getTodos(userID) {
  const response = await fetch(`http://localhost:3000/todos?user_id=${userID}`);
  const todoData = await response.json();
  return todoData;
}

async function getTodosConcurrently(userIDs) {
  const size = 5;
  let startIndex = 0;
  const todoPromises = [];

  while (startIndex < userIDs.length) {
    const endIndex = startIndex + size;
    const fiveUserIDs = userIDs.slice(startIndex, endIndex);

    const currentPromises = fiveUserIDs.map((item) => getTodos(item));

    todoPromises.push(Promise.all(currentPromises));
    startIndex = endIndex;

    if (startIndex < userIDs.length) {
      await new Promise((resolve) => setTimeout(resolve, 1000));
    }
  }
  return Promise.all(todoPromises);
}

async function main() {
  const userID = await getUsers();
  const userIDs = Object.keys(userID["users"]);
  const todos = await getTodosConcurrently(userIDs);

  result = [];
  for (let i = 0; i < userID.users.length; i++) {
    resultObj = {};
    resultObj.id = userID.users[i].id;
    resultObj.name = userID.users[i].name;
    result.push(resultObj);
  }
  let i = 0;
  for (let j = 0; j < todos.length; j++) {
    for (let index = 0; index < todos[j].length; index++) {
      console.log(index);
      const completed = todos[j][index].todos.filter(
        (todo) => todo.isCompleted
      );
      result[i].numTodosCompleted = completed.length;
      i++;
    }
  }
  console.log(result);
}
main();

// write your code here
