const fetch = require("node-fetch");

files = [];

async function dirfetch(dirpath) {
  const response = await fetch(dirpath);
  const data = await response.json();
  return data;
}

async function main() {
  const response = await fetch("http://localhost:3000/");
  const data = await response.json();
  for (let key of data.items) {
    if (key.isDir == false) {
      files.push(key.name);
    } else {
      const data2 = await dirfetch(`http://localhost:3000/` + `${key.name}`);
      for (let key of data2.files) {
        files.push(key.name);
      }
    }
  }
  console.log(files);
}

main();
