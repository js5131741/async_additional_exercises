const fetch = require("node-fetch");

async function dataFetch(size) {
  result = [];
  for (let i = 0; i < size; i++) {
    row = [];
    for (let j = 0; j < size; j += 2) {
      const data1 = fetch(
        `http://localhost:3000/value?rowIndex=${i}&colIndex=${j}`
      );
      const data2 = fetch(
        `http://localhost:3000/value?rowIndex=${i}&colIndex=${j + 1}`
      );
      const [response1, response2] = await Promise.all([data1, data2]);
      const value1 = await response1.json();
      const value2 = await response2.json();
      row.push(value1.value, value2.value);
    }
    result.push(row);
  }
  return result;
}

async function main() {
  const response = await fetch("http://localhost:3000/initialize");
  const data = await response.json();
  const result = await dataFetch(data.size);
  console.log(result);
}
main();
